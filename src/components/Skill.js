import React from "react";
import {
  View,
  Text,
  StyleSheet
} from "react-native";

export default function Skill() {
  return (
    <View style={styles.container}>
      <Text>Skills Page</Text>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
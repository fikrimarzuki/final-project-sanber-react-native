import React from "react";
import {
  View,
  Text,
  StyleSheet
} from "react-native";

export default function Experience({ route, navigation }) {
  const { payload } = route.params;
  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <Text>Company: {payload.company}</Text>
        <Text>Job Title: {payload.title}</Text>
        <Text>Location: {payload.location}</Text>
        <Text>Description: {payload.description}</Text>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  wrapper: {
    paddingVertical: 20,
    marginVertical: 10,
    marginHorizontal: 20,
    backgroundColor: "#fff",
    flexDirection: "column",
    alignSelf: "stretch",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 11,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  }
});
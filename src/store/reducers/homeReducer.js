import {
  SET_GITHUB_JOBS,
  SET_OPEN_SKILLS,
  SET_LOADING
} from '../actionTypes'

const initialState = {
  loading: false,
  openSkills: [],
  githubJobs: []
}

export default function homeReducer (state=initialState, action) {
  if(action.type === SET_GITHUB_JOBS) {
    console.log(action.payload, "reducer");
    return ({ ...state, githubJobs: action.payload })
  }
  if(action.type === SET_OPEN_SKILLS) {
    return ({ ...state, openSkills: action.payload })
  }
  if (action.type === SET_LOADING) {
    return ({ ...state, loading: action.payload })
  }
  return state
}
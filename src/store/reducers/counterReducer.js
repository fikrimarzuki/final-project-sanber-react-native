import {
  INCREMENT,
  DECREMENT
} from '../actionTypes'

const initialState = {
  counter: 0
}

export default function counterReducer (state=initialState, action) {
  if(action.type === INCREMENT) {
    return ({ ...state, counter: state.counter + action.payload })
  }
  if(action.type === DECREMENT) {
    return ({ ...state, counter: state.counter - 1 })
  }
  return state
}
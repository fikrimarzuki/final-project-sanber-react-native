const axios = require("axios");
import { githubJobAPI, openSkillAPI } from "../../config/index";
import {
  SET_LOADING,
  SET_GITHUB_JOBS,
  SET_OPEN_SKILLS
} from '../actionTypes'

export function setLoading(payload) {
  return {
    type: SET_LOADING,
    payload
  }
}

export function setGithubJobs(payload) {
  return {
    type: SET_GITHUB_JOBS,
    payload
  }
}

export function setOpenSkills(payload) {
  return {
    type: SET_OPEN_SKILLS,
    payload
  }
}

export function getGithubJobs() {
  return function(dispatch) {
    dispatch(setLoading(true));
    console.log("masuk");
    axios.get("https://jobs.github.com/positions.json?location=new_york")
      .then(({ data }) => {
        console.log(data, "then");
        dispatch(setGithubJobs(data));
      })
      .catch(err => {
        console.log(err.response);
      })
      .finally(_ => {
        dispatch(setLoading(false));
      })
  }
}

export function getOpenSkills() {
  return function(dispatch) {
    dispatch(setLoading(true));
    openSkillAPI.get("/")
      .then(({ data }) => {
        dispatch(setOpenSkills(data));
      })
      .catch(err => {
        console.log(err.response);
      })
      .finally(_ => {
        dispatch(setLoading(false));
      })
  }
}

import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import reducer from './reducers/index'
// import reducer from "./reducers/homeReducer";

export default createStore(
  reducer,
  applyMiddleware(
    thunkMiddleware
  )
)
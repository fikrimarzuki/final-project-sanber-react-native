import React from "react";
import {
  View,
  Text,
  Image,
  FlatList,
  StyleSheet
} from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const skills = [
  {
    id: 1,
    skillName: "React Native",
    categoryName: "Framework / Library",
    iconName: "react",
    percentageProgress: "50%"
  },
  {
    id: 2,
    skillName: "Laravel",
    categoryName: "Framework / Library",
    iconName: "laravel",
    percentageProgress: "100%"
  },
  {
    id: 3,
    skillName: "JavaScript",
    categoryName: "Bahasa Pemrograman",
    iconName: "language-javascript",
    percentageProgress: "30%"
  },
  {
    id: 4,
    skillName: "Python",
    categoryName: "Bahasa Pemrograman",
    iconName: "language-python",
    percentageProgress: "70%"
  },
  {
    id: 5,
    skillName: "Vue",
    categoryName: "Framework / Library",
    iconName: "vuejs",
    percentageProgress: "80%"
  },
  {
    id: 6,
    skillName: "Git",
    categoryName: "Teknologi",
    iconName: "git",
    percentageProgress: "75%"
  },
  {
    id: 7,
    skillName: "Gitlab",
    categoryName: "Teknologi",
    iconName: "gitlab",
    percentageProgress: "60%"
  }
]

export default function Skills() {
  return (
    <View style={styles.container}>
      <View style={styles.title}>
        <Text style={styles.titleText}>SKILL</Text>
        <View
          style={{ marginTop: 10, borderBottomColor: '#B4E9FF', borderBottomWidth: 2}}
        ></View>
      </View>
      <View style={styles.contentSkill}>
        <FlatList
          style={styles.wrapper}
          data={skills}
          renderItem={({item})=><View style={styles.contentWrapper}>
              <View>
                <Text style={styles.skillName}>{item.skillName}</Text>
                <Text style={styles.skillCategory}>{item.categoryName}</Text>
              </View>
              <Text style={styles.skillPercentage}>{item.percentageProgress}</Text>
              <Icon name={item.iconName} size={60} color="#003366" style={{ paddingBottom: 15 }}/>
            </View>
          }
          keyExtractor={(skill)=>skill.id}
          // ItemSeparatorComponent={()=><View style={styles.wrapper}/>}
        />
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 60,
  },
  title: {
    alignSelf: "stretch",
    padding: 10,
    alignItems: "center"
  },
  titleText: {
    color: "#013366",
    fontSize: 40
  },
  category: {
    alignSelf: "stretch",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  categoryContent: {
    backgroundColor: "#B4E9FF",
    margin: 10,
    padding: 8,
    borderRadius: 5,
    minWidth: 110
  },
  categoryText: {
    color: "#013366",
    fontSize: 12,
    fontWeight: "bold",
    textAlign: "center"
  },
  contentSkill: {
    alignSelf: "stretch",
    flex: 1
  },
  contentWrapper: {
    backgroundColor: "#B4E9FF",
    margin: 10,
    borderRadius: 10,
    flexDirection: "row",
    minHeight: 100,
    alignItems: "center",
    justifyContent: "space-between",
    padding: 15,
    paddingHorizontal: 30
  },
  skillName: {
    fontSize: 24,
    color: "#013366",
    fontWeight: "bold"
  },
  skillCategory: {
    fontSize: 16,
    color: "#3EC6FF"
  },
  skillPercentage: {
    color: "#fff",
    fontSize: 48,
    fontWeight: "bold",
    textAlign: "right"
  }
});
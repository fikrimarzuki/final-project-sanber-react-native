import React from "react";
import {
  View,
  Text,
  StyleSheet
} from "react-native";

export default function Splash({navigation}) {
  React.useEffect(() => {
    setTimeout(() => {
      navigation.replace("Register");
    }, 3000);
  }, [])

  return (
    <View style={styles.container}>
      <Text style={styles.text}>PORTOFOLIO</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontSize: 40,
    fontWeight: "bold",
    textShadowColor: "#000000",
    textShadowOffset: {
      width: 0.5,
      height: 2.5
    },
    textShadowRadius: 3
  }
})

import React from "react";
import {
  View,
  Text,
  StyleSheet
} from "react-native";

export default function Certificates({ navigation }) {
  const certificates = [
    { id: 1, name: "Reactjs Web Frontend", issued: "September 2020", issuingOrganization: "Sanbercode" },
    { id: 2, name: "Fullstack Javascript Immersive Program", issued: "March 2020", issuingOrganization: "Hacktiv8 Indonesia" },
    { id: 3, name: "Memulai Pemrograman dengan Kotlin", issued: "June 2020", issuingOrganization: "Dicoding" },
    { id: 4, name: "Fullstack Web Development dengan Framework Codeigniter", issued: "November 2019", issuingOrganization: "Codepolitan" },
    { id: 5, name: "Belajar Pemrograman PHP", issued: "November 2019", issuingOrganization: "Codepolitan" }
  ]

  return (
    <View style={styles.container}>
      {
        certificates.map((el) => {
          return (
            <View key={el.id} style={styles.wrapper}>
              <Text style={styles.name}>{el.name}</Text>
              <Text style={styles.organization}>{el.issuingOrganization}</Text>
              <Text style={styles.issuedDate}>{el.issued}</Text>
            </View>
          )
        })
      }
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  wrapper: {
    paddingVertical: 20,
    marginVertical: 10,
    marginHorizontal: 20,
    backgroundColor: "#fff",
    flexDirection: "column",
    alignSelf: "stretch",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 11,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  name: {
    fontSize: 15
  },
  organization: {
    fontWeight: "bold",
    fontSize: 20
  }
});
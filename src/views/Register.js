import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableHighlight
} from 'react-native';

export default function Register({navigation}) {
  const handleRegister = () => {
    navigation.navigate("Login");
  }

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.formLabel}> Sign Up </Text>
      </View>
      <View>
        <TextInput
          placeholder="Username"
          style={styles.inputStyle}
        />
        <TextInput placeholder="Email" style={styles.inputStyle} />
        <TextInput
          secureTextEntry={true}
          placeholder="Password"
          style={styles.inputStyle}
        />
        <TextInput
          secureTextEntry={true}
          placeholder="Confirm Password"
          style={{...styles.inputStyle, marginBottom: 30 }}
        />
        <TouchableHighlight
          style={styles.button}
          underlayColor='#fff'
          onPress={handleRegister}
        >
          <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableHighlight>
      </View>
      <View style={styles.textNavigation}>
        <Text>Already have an account? </Text>
        <Text
          style={{ color: "#4973DE"}}
          onPress={() => navigation.navigate("Login")}
        >
          Sign In
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  formLabel: {
    fontSize: 40,
    fontWeight: "bold"
  },
  inputStyle: {
    marginTop: 20,
    width: 300,
    height: 40,
    paddingHorizontal: 10,
    borderRadius: 5,
    backgroundColor: 'rgba(196, 196, 196, 0.13)',
    borderColor: "#C4C4C4",
    borderWidth: 1,
    color: 'rgba(0, 0, 0, 0.55)'
  },
  formText: {
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff',
    fontSize: 20,
  },
  text: {
    color: '#fff',
    fontSize: 20,
  },
  button: {
    marginRight:40,
    marginLeft:40,
    marginTop:10,
    paddingTop:20,
    paddingBottom:20,
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor: "#698AE0"
  },
  buttonText: {
    color:'#fff',
    textAlign:'center'
  },
  textNavigation: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end",
    marginTop: 10
  }
});
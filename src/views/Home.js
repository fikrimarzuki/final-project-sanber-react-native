import React, { useEffect } from "react";
import {
  ScrollView,
  View,
  Text,
  TextInput,
  StyleSheet,
  Image
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { getGithubJobs, getOpenSkills, setLoading } from "../store/actionCreators/home";
import Icon from "react-native-vector-icons/MaterialIcons";

export default function Home() {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.homeReducer.loading);
  const githubJobs = useSelector(state => state.homeReducer.githubJobs);
  const openSkills = useSelector(state => state.homeReducer.openSkills);

  useEffect(() => {
    console.log("effect");
    dispatch(setLoading(false));
    dispatch(getGithubJobs());
    console.log(githubJobs, "api");
    // dispatch(getOpenSkills());
  }, [])

  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>NOW LOADING....</Text>
      </View>
    )
  } else {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.header}>
          <Icon style={styles.searchIcon} name="search" size={20} color="#000"/>
          <TextInput
            placeholder="Search Jobs"
            style={styles.input}
            underlineColorAndroid="transparent"
          />
        </View>
        <View style={styles.slider}>
          <Image
            source={require("../assets/images/Slider.png")}
            resizeMode="contain"
          />
        </View>
        {
          githubJobs && githubsJobs.length !== 0
            ? githubJobs.map((el, index) => {
                return <View key={index}>
                  <Text>{el}</Text>
                </View>
              })
            : <Text></Text>
        }
      </ScrollView>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    flex: 1
  },
  header: {
    marginHorizontal: 20,
    flexDirection: 'row',
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: 'white',
    borderColor: "#C4C4C4",
    borderWidth: 1,
    color: 'rgba(0, 0, 0, 0.55)'
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    width: 260,
    height: 40,
    paddingHorizontal: 10,
  },
  slider: {
    marginTop: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
  }
});
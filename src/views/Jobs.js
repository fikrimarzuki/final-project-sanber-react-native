import React from "react";
import {
  View,
  Text,
  StyleSheet
} from "react-native";

export default function Experience({ navigation }) {
  const experiences = [
    { id: 1, company: "Karya Hidup Sejahtera", title: "Marketing", location: "Yogyakarta", description: "Analyzes and creates a plan for engaging the target market and maintain good relations with customer" },
    { id: 2, company: "Hacktiv8 Indonesia", title: "Student", location: "Jakarta", description: `
    Hacktiv8 is a coding bootcamp that transforms a total beginner from any background into a full-stack web developer within 16 weeks.
    During my study at Hacktiv8, i have learned Pair Programming, HTML/CSS, Javascript ES6, Version Control with Git, Database SQL & NoSQL, Node JS, Express, Vue.js & Vuex, React, React-Native & Redux, Test-Driven Development (TDD), Deployment
    ` },
    { id: 3, company: "Pixel House Studio", title: "Software Engineer", location: "Bandung", description: "Develop pixel house product (managix) as a Front End developer" }
  ]

  const showDetail = payload => {
    navigation.navigate("Job", { payload })
  }

  return (
    <View style={styles.container}>
      {
        experiences.map((el) => {
          return (
            <View key={el.id} style={styles.wrapper}>
              <Text style={styles.company}>{el.company}</Text>
              <Text style={styles.jobTitle}>{el.title}</Text>
              <Text onPress={() => showDetail(el)} style={styles.textNavigation}>
                More Detail
              </Text>
            </View>
          )
        })
      }
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  wrapper: {
    paddingVertical: 20,
    marginVertical: 10,
    marginHorizontal: 20,
    backgroundColor: "#fff",
    flexDirection: "column",
    alignSelf: "stretch",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 11,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  company: {
    fontWeight: "bold",
    fontSize: 20
  },
  jobTitle: {
    fontSize: 15
  },
  textNavigation: {
    color: "blue",
    fontWeight: "bold"
  }
});
import React from "react";
import {
  View,
  Text,
  Image,
  StyleSheet
} from "react-native";

export default function Profile() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>About Me</Text>
      <View style={styles.profile}>
        <View style={styles.avatarWrapper}>
          <Image source={require('../assets/images/avatar.jpg')} style={styles.avatar}/>
        </View>
        <Text style={{ fontSize: 20, color: "#013366", fontWeight: "bold" }}>Muhamad Fikri Marzuki</Text>
        <Text style={{ fontSize: 15, color: "#3fc5fc", fontWeight: "bold" }}>Software Developer</Text>
      </View>
      <View style={styles.education}>
        <Text style={styles.subTitle}>Education</Text>
        <View
          style={{ borderBottomColor: 'black', borderBottomWidth: 1}}
        ></View>
        <View style={styles.educationWrapper}>
          <View>
            <Text style={styles.company}>Gadjah Mada University</Text>
            <Text style={styles.jobTitle}>Agroindustrial Technology</Text>
          </View>
          <Text>2011 - 2017</Text>
        </View>
      </View>
      <View style={styles.contact}>
        <View>
          <Text style={styles.subTitle}>Social Media</Text>
          <View
            style={{ borderBottomColor: 'black', borderBottomWidth: 1}}
          ></View>
        </View>
        <View style={styles.contactContent}>
          <View style={styles.socialMedia}>
            <Image source={require('../assets/images/twitter.png')} style={styles.icon} />
            <Text style={{ marginLeft: 10 }}>@fikrimarzuki</Text>
          </View>
          <View style={styles.socialMedia}>
            <Image source={require('../assets/images/github.png')} style={styles.icon} />
            <Text style={{ marginLeft: 10 }}>@fikrimarzuki</Text>
          </View>
          <View style={styles.socialMedia}>
            <Image source={require('../assets/images/gitlab.png')} style={styles.icon} />
            <Text style={{ marginLeft: 10 }}>@fikrimarzuki</Text>
          </View>
        </View>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    marginTop: 60,
    alignItems: "center",
    justifyContent: "center"
  },
  avatarWrapper: {
    resizeMode: 'contain',
    width: 150,
    height: 150,
    margin: 10
  },
  title: {
    fontSize: 40
  },
  avatar: {
    width: 150,
    height: 150,
    borderRadius: 150/2
  },
  icon: {
    width: 50,
    height: 50
  },
  portofolio: {
    backgroundColor: "#efefef",
    alignSelf: "stretch",
    marginTop: 50,
    marginBottom: 10,
    borderRadius: 10,
    padding: 10
  },
  portofolioContent: {
    flexDirection: "row",
    margin: 10
  },
  education: {
    alignSelf: "stretch",
    borderRadius: 10,
    padding: 10,
    fontSize: 14
  },
  educationWrapper: {
    marginVertical: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  contact: {
    backgroundColor: "#efefef",
    alignSelf: "stretch",
    borderRadius: 10,
    padding: 10
  },
  subTitle: {
    color: "#013366",
    fontSize: 20
  },
  contactContent: {
    alignItems: "center",
    margin: 10
  },
  socialMedia: {
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
    margin: 10
  }
});
const axios = require("axios");

module.exports = axios.create({
  baseURL: 'http://api.dataatwork.org/v1/'
});


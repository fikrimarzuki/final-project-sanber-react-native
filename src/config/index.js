const exchangeAPI = require("./exchangeRates");
const openSkillAPI = require("./openSkill");
const githubJobAPI = require("./githubJob");

export default {
  exchangeAPI,
  openSkillAPI,
  githubJobAPI
}

const axios = require("axios");

module.exports = axios.create({
  baseURL: 'https://jobs.github.com/'
});

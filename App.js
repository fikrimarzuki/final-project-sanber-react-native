import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import store from "./src/store";
import Splash from "./src/views/Splash";
import Login from "./src/views/Login";
import Register from "./src/views/Register";
import Home from "./src/views/Home";
import Profile from "./src/views/Profile";
import Skills from "./src/views/Skills";
import Jobs from "./src/views/Jobs";
import Job from "./src/components/Job";
import Certificates from "./src/views/Certificates";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function TabScreen() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#e91e63',
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: "Home",
          tabBarIcon: ({color, size}) => (
            <MaterialIcons name="home" color={color} size={size}/>
          )
        }}
      />
      <Tab.Screen
        name="Skills"
        component={Skills}
        options={{
          tabBarLabel: "Skills",
          tabBarIcon: ({color, size}) => (
            <MaterialIcons name="build" color={color} size={size}/>
          )
        }}
      />
      <Tab.Screen
        name="Certificates"
        component={Certificates}
        options={{
          tabBarLabel: "Certificates",
          tabBarIcon: ({color, size}) => (
            <MaterialIcons name="assignment" color={color} size={size}/>
          )
        }}
      />
      <Tab.Screen
        name="Jobs"
        component={Jobs}
        options={{
          tabBarLabel: "Jobs",
          tabBarIcon: ({color, size}) => (
            <MaterialIcons name="business-center" color={color} size={size}/>
          )
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: "Profile",
          tabBarIcon: ({color, size}) => (
            <MaterialIcons name="person" color={color} size={size}/>
          )
        }}
      />
    </Tab.Navigator>
  )
}

function Index() {
  return (
    <Provider store={store}>
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <StatusBar style="auto" />
      </View>
    </Provider>
  );
}

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen name="Splash" component={Splash} options={{
            headerShown: false
          }}/>
          <Stack.Screen name="Login" component={Login} options={{
            headerShown: false
          }}/>
          <Stack.Screen name="Register" component={Register} options={{
            headerShown: false
          }}/>
          <Stack.Screen name="Home" component={TabScreen} options={{
            headerShown: false
          }}/>
          <Stack.Screen name="Job" component={Job} options={{
            headerShown: false
          }}/>
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
